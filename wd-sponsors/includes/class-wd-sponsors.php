<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the dashboard.
 *
 * @link       http://www.webdevs.li
 * @since      1.0.0
 *
 * @package    Wd_Sponsors
 * @subpackage Wd_Sponsors/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, dashboard-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Wd_Sponsors
 * @subpackage Wd_Sponsors/includes
 * @author     Rupert Quaderer <contact@webdevs.li>
 */
class Wd_Sponsors {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Wd_Sponsors_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $wd_sponsors    The string used to uniquely identify this plugin.
	 */
	protected $wd_sponsors;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * The post type name.
	 *
	 * @since 1.0.0
	 * @access protected
	 * @var string $_post_type The post type name, used for slides.
	 */
	protected $_post_type = 'sponsors';

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the Dashboard and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		$this->wd_sponsors = 'wd-sponsors';
		$this->version = '1.0.0';
		$this->load_dependencies();
		$this->set_locale();
		$this->set_imagesizes();
		$this->define_admin_hooks();
		$this->define_public_hooks();
		$this->set_post_type();
	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Wd_Sponsors_Loader. Orchestrates the hooks of the plugin.
	 * - Wd_Sponsors_i18n. Defines internationalization functionality.
	 * - Wd_Sponsors_Admin. Defines all hooks for the dashboard.
	 * - Wd_Sponsors_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-wd-sponsors-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-wd-sponsors-i18n.php';

		/**
		 * The class responsible for defining all widgets
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-wd-sponsors-widgets.php';

		/**
		 * The class responsible for defining all actions that occur in the Dashboard.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-wd-sponsors-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-wd-sponsors-public.php';

		$this->loader = new Wd_Sponsors_Loader();

	}


	/**
	 * Define some new image sizes for the sponsores
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_imagesizes() {
		add_image_size( 'sponsors-thumb', 190 ); // set with to 190 pixels
	}



	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Wd_Sponsors_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Wd_Sponsors_i18n();
		$plugin_i18n->set_domain( $this->get_wd_sponsors() );

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the dashboard functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Wd_Sponsors_Admin( $this->get_wd_sponsors(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Wd_Sponsors_Public( $this->get_wd_sponsors(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_wd_sponsors() {
		return $this->wd_sponsors;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Wd_Sponsors_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

	/**
	 * Register post types
	 *
	 * @since     1.0.0
	 */
	public function set_post_type() {
		add_action( 'init', array( $this, 'register_post_type' ) );
	}

	/**
	 * Gets the post type.
	 *
	 * @since 1.0.0
	 *
	 * @return string The post type.
	 */
	public function get_post_type() {
		return apply_filters( 'wd-sponsors-post-type', $this->_post_type );
	}

	function register_post_type() {
		register_post_type(
			$this->_post_type,
			array(
				'labels' => array(
					'name' => __( 'Sponsoren', 'wd-sponsors' ),
					'singular_name' => __( 'Sponsoren', 'wd-sponsors' ),
					'add_new' => __( 'Sponsor hinzufügen'),
					'add_new_item' => __( 'Neuen Sponsor hinzufügen', 'wd-sponsors' ),
					'edit_item' => __( 'Sponsor bearbeiten', 'wd-sponsors' ),
					'new_item' => __( 'Neuer Sponsor', 'wd-sponsors' ),
					'all_items' => __( 'Alle Sponsoren', 'wd-sponsors' ),
					'view_item' => __( 'Sponsoren anzeigen', 'wd-sponsors' ),
					'search_items' => __( 'Suche Sponsoren', 'wd-sponsors' ),
					'not_found' =>  __( 'Keine Sponsoren gefunden', 'wd-sponsors' ),
					'not_found_in_trash' => __( 'Keine Sponsoren in Papierkorb gefunden', 'wd-sponsors' ),
					'parent_item_colon' => '',
					'menu_name' => __( 'Sponsoren', 'wd-sponsors' )
				),
				'public' => false,
				'publicly_queryable' => false,
				'show_ui' => true,
				'show_in_menu' => true,
				'query_var' => true,
				'rewrite' => array( 'slug' => 'sponsor' ),
				'capability_type' => 'post',
				'has_archive' => false,
				'hierarchical' => false,
				'menu_position' => null,
				'supports' => array( 'title'),
				'menu_icon' => 'dashicons-networking'
			)
		);
	}




}
