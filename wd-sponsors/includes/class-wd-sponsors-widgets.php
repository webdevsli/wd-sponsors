<?php
/**
 * Plugin Name: Tag Cloud Widget
 */

add_action( 'widgets_init', 'wd_load_sponsor_widget' );

function wd_load_sponsor_widget() {
    register_widget( 'wd_sponsor_widget' );
}

class wd_sponsor_widget extends WP_Widget {
    /**
     * Widget setup.
     */
    function wd_sponsor_widget() {
        /* Widget settings. */
        $widget_ops = array( 'classname' => 'wd_sponsor_widget', 'description' => __('Anzeige der Sponsoren.', 'wd_sponsor_widget') );

        /* Widget control settings. */
        $control_ops = array( 'width' => 250, 'height' => 350, 'id_base' => 'wd_sponsor_widget' );

        /* Create the widget. */
        $this->WP_Widget( 'wd_sponsor_widget', __('Sponsoren', 'wd_sponsor_widget'), $widget_ops, $control_ops );
    }

    /**
     * How to display the widget on the screen.
     */
    function widget( $args, $instance ) {
        extract( $args );

        /* Our variables from the widget settings. */
        $title = apply_filters('widget_title', $instance['title'] );
        $number = $instance['number'];

        /* Before widget (defined by themes). */
        echo $before_widget;

        /* Display the widget title if one was input (before and after defined by themes). */
        if ( $title )
            echo $before_title . $title . $after_title;

        ?>
                <div class="sponsors left relative">
                    <ul>
                    <?php
                        $posts = get_posts(array(
                            'numberposts' => -1,
                            'post_type' => 'sponsors'
                        ));
                    ?>
                    <?php foreach( $posts as $post ) : ?>
                    <?
                        $image = get_field("sponsor_image",$post->ID);
                        $imageSize = 'sponsors-thumb'; // (thumbnail, medium, large, full or custom size)
                        $image_information = wp_get_attachment_image_src( $image, $imageSize);
                    ?>
                    <li>
                    <? if($image) : ?>
                        <a href="<?php echo get_field("sponsor_link",$post->ID) ?>" title="<?php echo get_the_title($post->ID); ?>" target="_blank">
                            <img src="<?php echo $image_information[0]; ?>" alt="<?php echo get_the_title($post->ID); ?>" />
                        </a>&nbsp;
                    <? else : ?>
                        <a href="<?php echo get_field("sponsor_link",$post->ID) ?>" title="<?php echo get_the_title($post->ID); ?>" target="_blank">
                            <?php echo get_the_title($post->ID); ?>
                        </a>&nbsp;
                    <? endif; ?>
                    </li>
                    <?php endforeach; ?>
                    </ul>
                </div>


        <?php

        /* After widget (defined by themes). */
        echo $after_widget;
    }

    /**
     * Update the widget settings.
     */
    function update( $new_instance, $old_instance ) {
        $instance = $old_instance;

        /* Strip tags for title and name to remove HTML (important for text inputs). */
        $instance['title'] = strip_tags( $new_instance['title'] );

        return $instance;
    }


    function form( $instance ) {

        /* Set up some default widget settings. */
        $defaults = array( 'title' => 'Titel', 'number' => 40);
        $instance = wp_parse_args( (array) $instance, $defaults ); ?>

        <!-- Widget Title: Text Input -->
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>">Titel:</label>
            <input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" style="width:90%;" />
        </p>

    <?php
    }
}

?>