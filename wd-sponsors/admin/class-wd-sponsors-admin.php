<?php

/**
 * The dashboard-specific functionality of the plugin.
 *
 * @link       http://www.webdevs.li
 * @since      1.0.0
 *
 * @package    Wd_Sponsors
 * @subpackage Wd_Sponsors/admin
 */

/**
 * The dashboard-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the dashboard-specific stylesheet and JavaScript.
 *
 * @package    Wd_Sponsors
 * @subpackage Wd_Sponsors/admin
 * @author     Rupert Quaderer <contact@webdevs.li>
 */
class Wd_Sponsors_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $wd_sponsors    The ID of this plugin.
	 */
	private $wd_sponsors;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @var      string    $wd_sponsors       The name of this plugin.
	 * @var      string    $version    The version of this plugin.
	 */
	public function __construct( $wd_sponsors, $version ) {

		$this->wd_sponsors = $wd_sponsors;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the Dashboard.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Wd_Sponsors_Admin_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Wd_Sponsors_Admin_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->wd_sponsors, plugin_dir_url( __FILE__ ) . 'css/wd-sponsors-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the dashboard.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Wd_Sponsors_Admin_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Wd_Sponsors_Admin_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->wd_sponsors, plugin_dir_url( __FILE__ ) . 'js/wd-sponsors-admin.js', array( 'jquery' ), $this->version, false );

	}

}
