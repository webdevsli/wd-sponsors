<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * Dashboard. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://www.webdevs.li
 * @since             1.0.0
 * @package           Wd_Sponsors
 *
 * @wordpress-plugin
 * Plugin Name:       WD Sponsors
 * Plugin URI:        https://bitbucket.org/webdevsli/wd-sponsors
 * Description:       This is a small view of sponsores who can add you in the backend
 * Version:           1.0.0
 * Author:            Rupert Quaderer
 * Author URI:        http://www.webdevs.li
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       wd-sponsors
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-wd-sponsors-activator.php
 */
function activate_wd_sponsors() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wd-sponsors-activator.php';
	Wd_Sponsors_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-wd-sponsors-deactivator.php
 */
function deactivate_wd_sponsors() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wd-sponsors-deactivator.php';
	Wd_Sponsors_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_wd_sponsors' );
register_deactivation_hook( __FILE__, 'deactivate_wd_sponsors' );

/**
 * The core plugin class that is used to define internationalization,
 * dashboard-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-wd-sponsors.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_wd_sponsors() {

	$plugin = new Wd_Sponsors();
	$plugin->run();

}
run_wd_sponsors();
