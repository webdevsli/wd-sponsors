<?php

/**
 * Fired during plugin activation
 *
 * @link       http://www.webdevs.li
 * @since      1.0.0
 *
 * @package    Wd_Sponsors
 * @subpackage Wd_Sponsors/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Wd_Sponsors
 * @subpackage Wd_Sponsors/includes
 * @author     Rupert Quaderer <contact@webdevs.li>
 */
class Wd_Sponsors_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
